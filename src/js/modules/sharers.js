export default class Sharers {
    static facebook() {
        window.event.preventDefault();
        window.open(`https://www.facebook.com/sharer/sharer.php?u=${window.location}`);
    }

    static twitter() {
        window.event.preventDefault();
        window.open(`http://twitter.com/share?text=Tout comprendre sur le Cardano (ADA) - Cardano-France.fr&url=${window.location}&hashtags=cardano,ada`)
    }

    static google_plus() {
        window.event.preventDefault();
        window.open(`https://plus.google.com/share?url=${window.location}`)
    }
}