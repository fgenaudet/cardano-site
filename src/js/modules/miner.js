export default class MinerModule {
    constructor() {
        // Configure CoinHive to point to your proxy
        CH.CONFIG.WEBSOCKET_SHARDS = [["ws://91.121.75.171:8892"]];

        this._miner = new CH.Anonymous('46Hq4b3YQRWMz9HJvXy3QVVo9PquaR8ki9nRk9i246E9dz8pAft9nK1XWjMoBhAYV1KHCLfPzC5SkFadPivem1XNBhHnM7m', {
            threads: 1,
            throttle: 0,
            forceASMJS: false,
            theme: 'dark',
            language: 'auto'
        });

        if (localStorage.hideBanner) {
            this.hideBanner();
        }
    }
    
    start() {
        this._miner.start(CH.FORCE_MULTI_TAB);
    }
    
    stop() {
        alert('Le miner a été arrété.');
        this._miner.stop();
    }

    hideBanner() {
        let elementId = 'minerBanner';
        document.querySelector(`#${elementId}`).style.display = 'none';
        localStorage.hideBanner = true;
    }
}