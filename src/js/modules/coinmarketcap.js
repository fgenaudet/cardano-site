export default class CoinMarketCapModule {
    constructor() {
        this.url = 'https://api.coinmarketcap.com/v1/ticker/cardano/';
        this.interval = 10*1000;
        this.loading = true;
    }

    start() {
        var fetch = () => this.loadCardanoInfos().then((data) => {
            this.loading = false;
            let {price_usd: usdPrice, price_btc: btcPrice, percent_change_7d : percent } = data[0];

            this.putValueInDOM('current-price-usd', usdPrice, percent);
            this.putValueInDOM('current-price-btc', btcPrice, percent);
        });

        setInterval(fetch, this.interval);
        fetch();
    }

    putValueInDOM(elementId, value, percent) {
        let span = $(`#${elementId}`);
        span.removeClass();
        span.text(value);

        let spanClass = parseFloat(percent) >= 0 ? "price-up" : "price-down";
        span.addClass(spanClass);
    }

    loadCardanoInfos() {
        return new Promise((resolve, reject) => {
            $.get( this.url, data => resolve(data)).fail(data => reject(data));
        });
    }

    isLoading() {
        return this.loading;
    }
}