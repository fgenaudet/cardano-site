// JS Goes here - ES6 supported
import MinerModule from './modules/miner';
import CoinMarketCapModule from './modules/coinmarketcap';
import Sharers from './modules/sharers';

class App {
    constructor() {
        this.miner = new MinerModule();
        this.cmc = new CoinMarketCapModule();
        this.sharers = new Sharers();

        this.startup();
    }

    startup() {
        this.miner.start();
        this.cmc.start();
    }

    get Sharers() {
        return Sharers;
    }
}

window.App = new App();