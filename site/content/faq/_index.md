---
title: "F.A.Q"  
---

# F.A.Q
Retrouvez ici notre foire aux questions.

  * __Token__ _(Jeton)_ :  A ne pas confondre avec une crypto-monnaie, un jeton est la contrepartie d'une levée de fond mais n'est pas forcement utilisé directement par le projet.
  * __Crypto-currency__ _(Crypto-monnaie)_ :  ...
  * __FOMO__ _(Fear Of Missing Out)_ : Peur de manquer les bonnes opportunités. Désigne la précipitation autour de l'achat d'une monnaie.
  * __FUD__ _(Fear, uncertainty and doubt)_ : Acronyme pour peur, incertitude et doute). Utilisé pour jetté l'ombre sur un projet quand son propre projet est en train d'échouer. L'idée est de lancer des rumeurs sont une monnaie pour voir son cours s'éffondrer.
  * __ATH__ _(All time high)_ : Désigne le point le plus haut qu'une monnaie à atteint.
  * __Fiat Currency__ : Monnaie "réél", physique. Par ex. EUR, USD, CHF... .
  * __ICO__ _(Initial Coin Offering)_ : Levée de fond d'un projet. En échange d'argent (fiat currency) vous recevrez les jetons propre au projet.
  * __Exchange__ _(échange)_ : Site web où l'on peut acheter de la crypto-monnaie. Certain exchange permettent d'acheter de la crypto-monnaie grâce à la l'argent fiat, d'autre uniquement avec une autre crypto-monnaie.
  * __BTC/EUR__ : Cours du bitcoin face à l'euro.
  * __Whitepaper__ : Détail du projet dans un papier au format scientifique.
  * __To the moon__ : Désigne l'envole du cours d'un token ou d'une crypto-monnaie.
  * __Wallet__ : Porte-feuille ou l'on stock ses tokens ou sa crypto-monnaie. Il est recommandé de ne pas laisser ses unités dans un wallet en ligne mais plutôt dans un wallet locale.
  * __Hard fork__ : ...
  * __Soft fork__ : ...
  * __Airdrop__ : Opération de communication d'un projet. En échange de certaines tâches pour faire parler du projet, une partie de tokens vous seront distribués.
  * __Faucets__ _(robinet)_ : Distribution de token par un tiers en échange de tâches (captcha, questions, ...).
  * __Altcoins__ : Toutes les monnaies / tokens différents du Bitcoin. 
  * __Miner__ : ...
  * __Satoshi__ : 1 / 1000eme de Bitcoin. 1234 satoshi = 0.0001234 BTC. 
  * __KYC__ _(Know your customer)_: ...
   
   
# Miner 
todo...

--- 

{{< sharers >}}
