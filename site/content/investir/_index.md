---
title: "Investir"
menu:
  raisons:
    title: "Les Raisons"
    weight: 0
    url: "#les-raisons"
  plateformes:
    title: "Les Plateformes"
    weight: 10
    url: "#les-plateformes"
  wallet:
      title: "Le Wallet"
      weight: 20
      url: "#le-wallet"
---

# Les Raisons

La Blockchain est une technologie révolutionnaire.
Cependant, les solutions actuelles sont entravées par des problèmes tels qu'un manque de surveillance réglementaire, des logiciels expérimentaux avec une sécurité non prouvée,
une mauvaise gouvernance corrolée à une absence de vision à long terme qui peuvent nuirent à l'èvolutivité de la blockchain.
Autant de points d'attention qui sont le fer de lance de Cardano.

- **une nouvelle ère** :
La blockchain, technologie encore méconnue du grand public il y 1 an, est en plein développement. Le marché des crypto-monnaies a connu une formidable croissance : le marketcap global est passé de 17 milliards de dollars en Janvier 2017 à 813 milliards de dollars en Janvier 2018 et ne cessera de croitre. Devant cette ferveur les principaux exchanges ont dû geler les inscriptions suite à la recrudescence de nouveaux utilisateurs. De plus en plus de groupes et établissements financier s’impliquent massivement dans cette technologie. Néanmoins, Nous sommes aujourd’hui dans une bulle : il existe plus de 1400 crypto-monnaies ou tokens, et de nombreux projets émergent tous les jours grâce notamment aux ICOs. Cette bulle finira à terme par exploser, mais ne signifiera pas pour autant la mort de la technologie blockchain. Seuls les projets les plus sérieux, à haute valeur ajoutée, répondants aux besoins aussi bien des organismes de régulation qu’à ceux des utilisateurs resteront et subirons une très forte valorisation. C’est le cas de Cardano.

- **Les besoins liés à la régulation** :
Depuis l’explosion des volumes d’investissement dans les crypto-monnaies et l’intérêt croissants du grand public, les organismes de régulation et gouvernements entament une réflexion sur l'adoption de restrictions quant à l’utilisation des blockchaines. C’est le cas de la Corée du Sud qui a annoncé une interdiction des échanges anonymes de crypto-monnaies, ou encore de l’Ukraine qui a introduit une nouvelle loi mettant l’écosystème de la crypto-monnaie locale sous la supervision de la banque centrale du pays.
Qui plus est si la technologie blockchain vient à bouleverser des industries réglementées telles que la finance, des outils réglementaires doivent être créés. Cardano résout ce besoin à travers son architecture multicouche innovante tout en préservant les droits individuels à la vie privée dans les transactions financières, et sera une solution incontournable dans l’hypothèse où l’étau législatif pourrait amener certaines crypto-monnaies à être restreintes.


- **modèle de gouvernance démocratique** :

- **prouvé mathématiquement sécurisé** :

- **Technologiqment au delas des autres blockchain** :

- **Cartes de paiment**

---

# Les Plateformes
Le **Lorem Ipsum** est simplement du faux texte employé dans la composition et la mise en page avant impression. 
Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. 
Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. 
Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

---

# Le Wallet
Le **Lorem Ipsum** est simplement du faux texte employé dans la composition et la mise en page avant impression. 
Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. 
Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. 
Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

---

{{< sharers >}}