---
title: "A Propos"  
---

# Qui Sommes-Nous ?

Hello, tout d'abord merci de nous rejoindre sur ce site dédié à Cardano et à sa monnaie l'ADA.
Nous sommes Michaël et Florian, au départ deux passionés d'informatique qui se sont pris au jeu des crypto-monnaie et des projets liés à la blockchaine.
Souhaitant réelement comprendre ce qu'il se cache derrière tout ce buzz nous avons commencer à nous intéresser à tous ces projets en essayant de comprendre leur liens avec la blockchaine.
Dans cet effort de recherche, le projet Cardano nous à sembler sortir du lot par son approche scientifique et son ambition de régulation.
Nous avons donc souhaiter rendre accessible ce projet à la communauté française, technique ou non technique.
 
# Nous Contacter ?
 
Pour nous contacter, envoyer nous un e-mail à contact@cardano-france.fr.




