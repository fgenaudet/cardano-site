---
menu:
  lequipe:
    title: "L'équipe"
    weight: 0
    url: "#l-équipe"
  besoin:
    title: "Le Besoin"
    weight: 10
    url: "#le-besoin"
  monnaie:
    title: "La Cryptomonnaie (ADA)"
    weight: 20
    url: "#la-cryptomonnaie-ada"
  roadmap:
    title: "La Roadmap"
    weight: 30    
    url: "#la-roadmap"
---

# L'équipe

Le **Lorem Ipsum** est simplement du faux texte employé dans la composition et la mise en page avant impression. 
Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. 
Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. 
Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.
[En savoir plus](../le-projet/#details)

---

# Le Besoin
Le **Lorem Ipsum** est simplement du faux texte employé dans la composition et la mise en page avant impression. 
Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. 
Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. 
Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

---

# La Cryptomonnaie (ADA)
Le **Lorem Ipsum** est simplement du faux texte employé dans la composition et la mise en page avant impression. 
Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. 
Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. 
Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

---

# La Roadmap
Le **Lorem Ipsum** est simplement du faux texte employé dans la composition et la mise en page avant impression. 
Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. 
Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. 
Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.
{{< roadmap nodeStart="true" >}}
{{< roadmap type="start past" date="29 Sept 2017" title="Lancement de Cardano SL Mainnet" progress="100">}}
{{< roadmap type="futur" date="-" title="Delegation Ouroboros Ouverte" progress="75">}}
{{< roadmap type="futur" date="-" title="Transcations multisignature" progress="35">}}
{{< roadmap type="futur" date="-" title="Amélioration gestion serveur des portefeuilles" progress="40">}}
{{< roadmap type="futur" date="-" title="Incitation aux consensus et frais" progress="45">}}
{{< roadmap type="futur" date="-" title="Signature quantiques résistantes" progress="50">}}
{{< roadmap type="futur" date="-" title="Support client léger" progress="20">}}
{{< roadmap type="futur" date="-" title="Adresses humainement intelligible" progress="20">}}
{{< roadmap type="end" date="-" title="Fin du projet" progress="15">}}
{{< roadmap nodeEnd="true" >}}

--- 

{{< sharers >}}