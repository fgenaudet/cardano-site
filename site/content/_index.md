---
title: "Acceuil"
---

# Mise en garde

Cardano ADA est une cryto-monaie à fort potentiel et nous croyons avec enthousiasme à son essor et à sa valorisation dans les mois à venir.
Néanmoins, ce site a pour vocation de vous apporter des connaissances autour d'une technologie plus que prometteuse, et ne constitue en aucun cas une recommandation d'investissement.

---

# Introduction à la Blockchain

Crypto-monnaie et Blockchain, vous en avez entendu parler mais vous n'y comprenez rien ? On vous explique tout !

Prenons l'exemple de la banque : vous souhaitez envoyer 200 € à votre ami Jean. Vous contacter votre banque (téléphone, site web, application mobile, chèque, ...) et vous lui demandez d'effectuer la transaction.
Votre banque va donc écrire dans sa **base de données**: _John envoie 200€ à Jean_. 
Et cette information n'est visible **que de votre banque**. 
En cas de litige pas exemple, seul l'information que votre banque à écrite est considéré comme **véritable**.
_En réalité c'est un peu plus compliqué que cela, mais l'idée est là._

Maintenant imaginons que le livre de compte de votre banque soit en fait **partager par tout le monde** et que vous n'ayez plus besoin de **passer par votre banque** ? Comment est-ce possible ?
C'est l'idée derrière la blockchain: le partage de l'information par un grand nombre de personne (ou machine) et la facilitation du partage et de l'accès à l'information.

---

# Fonctionnement de la Blockchain

La blockchain correspond (dans notre exemple) à un grand livre de compte, organisée en block (un block = une page).

Elle est répliquée (dupliquée) entièrement sur des centaines de milliers de machine (en réalité n'importe qui peut faire office de "nœud").

Chaque fois qu'une nouvelle transaction arrive (Jean envoie 3 Bitcoins à John), **le réseau la valide** (est-ce que Jean possède au moins 3 BTC ? Est-ce que John existe ? ...) l'insère dans un block.

Une fois que le block est plein, des machines spéciales _(appelée miner)_, s'occupe de valider le block complet, puis de créer un nouveau block pour accueillir les prochaines transactions. 

---

# Avantages de la Blockchain

Le premier avantage est la sécurité des informations inscrites dans la blockchain : **elles sont inviolables**.
 
Si un pirate tente de modifier une transaction sur un nœud, les autres nœuds du réseau vont lui dire que cette transaction est fausse puisqu'ils en ont une copie différente.
Par la suite, le réseau va corriger de lui-même le nœud corrompu.
On appelle cela un système décentralisé c.-à-d. qu'il n'y a pas qu'une seul serveur (par ex. celui de la banque) qui connait l'information.

L'autre avantage de la blockchain est **l'anonymisation de l'information**. 

En effet dans notre exemple, la transaction qui sera écrite dans la blockchain ressemblera à cela :
0x34389fd98989 donne 3 BTC à 0x4343989dfdeed. 
La transaction utilise l'adresse (l'identité) du portefeuille de Jean et de John. Et normalement, personne à part A (et les personnes à qui il a donné l'information) sait que son adresse est 0x34389fd98989.
**Impossible** donc pour les noeuds de la blockchain (ou des tiers) de connaitre le solde de votre compte en banque.

**Note** : L'un des but de Cardano est de conserver ces avantages tout en y apportant de la transparence en ouvrant la porte aux différents organismes de régulation bancaire.

Nous vous avons présenté un exemple lié à la banque car c'est le plus simple à expliquer, cependant la technologie blockchain peut être utilisé dans beaucoup d'autres domaines,
comme par exemple la musique (Jean a créé cette mélodie), la presse (Jean a écrit ce contenu), et bien d'autres. 

---

# Le Projet Cardano
Cardano, qui a été créé par la société de développement IOHK, est une infrastructure qui supporte le jeton appelé ADA. Le PDG de IOHK étant Charles Hoskinson, le co-fondateur d'Ethereum.
Cardano utilise un nouveau type d'algorithme Proof-of-Stake (Preuve d'enjeu) nommé Ouroboros, rendant les transactions rapides et hautement sécurisées. Ce sera aussi, tout comme Ethereum, une plateforme de smart contracts et d'applications décentralisées.
ADA est la Crypto-monnaie utilisée sur la plateforme Cardano pour les transferts ou paiements, et possède déjà son propre wallet (Daedalus). 

Le projet va s'améliorer, avec prochainement la Cardano Credit card ainsi que l'ouverture d'une vingtaine de distributeurs permettant l'achat de ADA au Japon.

[En savoir plus](../le-projet)

---

# Investir dans l'ADA
Il existe plusieurs plateformes permettant d'acheter de l'ADA, en achetant au préalable une autre crypto-monnaie. 
On vous explique tout dans le chapitre dédié à l'investissement. On vous parle aussi de la fiscalité lié aux crypto-monnaies et ce que vous devez faire pour être en règle.

[En savoir plus](../investir)

---

# En direct
Retrouver les actualités de Cardano, du cours, des analyses technique du Cardano et tout ce que vous avez besoin de savoir pour être au top sur le sujet.

[En savoir plus](../en-direct)

---

# F.A.Q
Ici pas du **FUD**, pas de **FOMO** ! Le Cardano n'est pas un **token** mais bien une **crypto-currency** que l'on ne peut pas acheter avec du **fiat**. Vous n'avez pas tout compris, pas d'inquiétude vous trouverez notre lexique dans notre page F.A.Q.

[En savoir plus](../faq)

---

# A Propos
Nous sommes 2 passionnés d'informatique qui avons commencé par nous intéresser de très près à la technologie **Blockchain** avant de nous pencher sur les différents projets de crypto-monnaies. 
Parmi tous ces projets, l'approche de Cardano nous à impressionner et nous avons décider de vous en parler sur ce site. 

[En savoir plus](../a-propos)

---

{{< sharers >}}